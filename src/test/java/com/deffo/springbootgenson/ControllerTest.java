package com.deffo.springbootgenson;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest
class ControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	@DisplayName("Test endpoint get Messages")
	void send_messages() throws Exception{
		this.mockMvc.perform(get("/api/messages")
		.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].id").value("1"))
				.andExpect(jsonPath("$.[0].payload").value("First message"))
				.andExpect(jsonPath("$.[1].id").value("2"))
				.andExpect(jsonPath("$.[1].payload").value("Second message"));
	}

	@Test
	@DisplayName("Test endpoint post Messages")
	void save_messages() throws Exception{
		this.mockMvc.perform(post("/api/messages")
				.contentType(MediaType.APPLICATION_JSON)
				.content("[{\"id\":1,\"payload\":\"First message\"},{\"id\":2,\"payload\":\"Second message\"}]"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].id").value("1"))
				.andExpect(jsonPath("$.[0].payload").value("First message"))
				.andExpect(jsonPath("$.[1].id").value("2"))
				.andExpect(jsonPath("$.[1].payload").value("Second message"));
	}

}
