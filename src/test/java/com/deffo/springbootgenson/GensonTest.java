package com.deffo.springbootgenson;

import com.owlike.genson.Genson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GensonTest {

    private static Genson genson = new Genson();

    @Test
    void should_deserialize_message() {
        SpringBootGensonApplication.Message message = genson.deserialize("{\"id\":1,\"payload\":\"first message\"}",SpringBootGensonApplication.Message.class);
        Assertions.assertEquals(message.getId(),1L,"Message Id should be equal to 1L");
        Assertions.assertEquals(message.getPayload(),"first message","Message payload should be equal to first message");
    }
}
