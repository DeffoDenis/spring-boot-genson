package com.deffo.springbootgenson.config;

import com.owlike.genson.ext.spring.GensonMessageConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * This class helps Spring boot during the serialisation/deserialisation process
 * forcing it to use Gson library.
 * Without this class, we faces an HttpMediaTypeNotAcceptableException exception
 */

@EnableWebMvc
@Configuration
public class MessageConverterConfiguration implements WebMvcConfigurer {

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new GensonMessageConverter());
    }


}
