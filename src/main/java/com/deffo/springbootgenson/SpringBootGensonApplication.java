package com.deffo.springbootgenson;

import lombok.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Deffo Denis
 * @version 1.0
 */
@SpringBootApplication
@RestController
@RequestMapping("api/")
public class SpringBootGensonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootGensonApplication.class, args);
	}

	@GetMapping("/messages")
	List<Message> getAllMessages(){
		return this.deliverMessages();
	}

	@PostMapping("/messages")
	List<Message> saveAllMessages(@RequestBody List<Message> messageList){
		return messageList;
	}

	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@Getter
	@Setter
	static class Message {
		private Long id;
		private String payload;
	}

	private List<Message> deliverMessages(){
		return Stream.of(Message.builder().id(1L).payload("First message").build(),Message.builder().id(2L)
				.payload("Second message").build()).collect(Collectors.toList());
	}

}
