# `Implementing Spring boot project Using Genson instead of Jackson library`

Spring Boot uses Jackson as the default library to serialize and deserialize Java objects to JSON. Adding “spring-boot-starter Spring Boot Starters in your application, it will be included to your classpath. Sometimes you may like to use other API than what is available as part of the Spring Boot auto-configuration. 
That's where Genson Library comes in the picture, This project is a kind of P.O.C whose aim is to evaluate how the Genson library can be integrated in spring boot environment.

### What is Genson library?
Genson is a complete json <-> java conversion library, providing full databinding, streaming and much more.

The online documentation is available here : **https://github.com/owlike/genson**

### How did we manage to use Genson Library instead of Jackson in a Spring boot project?

###### 1. Adding Genson library dependency in pom.xml
``<!-- genson dependency -->
<dependency>
    <groupId>com.owlike</groupId>
    <artifactId>genson</artifactId>
    <version>1.6</version>
</dependency> ``

###### 2. Excluding Jackson library dependency in pom.xml
Jackson Library is part of Spring-boot-starter-web dependency, so in order to remove it , You just have to exclude it.


``
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    <!-- Exclude the default Jackson dependency -->
    <exclusions>
        <exclusion>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-json</artifactId>
        </exclusion>
    </exclusions>
</dependency>
``
###### 3. Extending WebMvcConfigurer class in order to enable spring boot to use Genson library in the serialisation/deserialisation process.
Jackson has built-in support in Spring MVC whereas Genson doesn't.This configuration class helps us avoiding the HttpMediaTypeNotAcceptableException exception.


``
@EnableWebMvc
@Configuration
public class MessageConverterConfiguration implements WebMvcConfigurer {

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new GensonMessageConverter());
    }
}
``

###### That's all folks, your project is ready to work with Genson. So have fun!
